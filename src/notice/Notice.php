<?php

namespace addons\notice;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Notice extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name'    => 'notice',
                'title'   => '公告管理',
                'icon'    => 'fa fa-bullhorn',
                'sublist' => [
                    ["name"  => "notice/index","title" => "查看"],
                    ["name"  => "notice/add","title" => "添加"],
                    ["name"  => "notice/edit","title" => "编辑"],
                    ["name"  => "notice/del","title" => "删除"],
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete("notice");
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable("notice");
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable("notice");
        return true;
    }

}
