<?php

return [
    'Id'               => 'ID',
    'Channel_id'       => '所属渠道',
    'Createtime'       => '创建时间',
    'Total_fee'        => '金额',
    'Withdraw_type'    => '提现方式',
    'Withdraw_account' => '提现账号',
    'Status'           => '状态',
    'Status 0'         => '待审核',
    'Status 1'         => '已审核',
    'Status 2'         => '已驳回',
    'Op_admin_id'      => '操作员',
    'Updatetime'       => '处理时间',
    'Remark'           => '备注',
    'Channel.truename' => '真实姓名',
    'Admin.nickname'   => '昵称'
];
