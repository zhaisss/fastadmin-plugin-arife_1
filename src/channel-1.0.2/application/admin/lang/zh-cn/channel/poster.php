<?php

return [
    'Id'           => 'ID',
    'Title'        => '标题',
    'Poster_image' => '海报底图',
    'Url'          => '推广链接',
    'Xpos'         => 'X坐标',
    'Ypos'         => 'Y坐标',
    'Createtime'   => '创建时间',
    'Status'       => '启用状态',
    'Status 0'     => '禁用',
    'Status 1'     => '启用'
];
