<?php

namespace app\admin\model\channel;

use think\Model;


class User extends Model
{

    

    

    // 表名
    protected $name = 'channel_user';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function channel()
    {
        return $this->belongsTo('app\admin\model\channel\Index', 'channel_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function cuser()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
