<?php

namespace app\admin\model\channel;

use think\Model;

class Withdraw extends Model
{

    

    

    // 表名
    protected $name = 'channel_withdraw';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text'
    ];
    

    
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1'), '2' => __('Status 2')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function channel()
    {
        return $this->belongsTo('app\admin\model\channel\Index', 'channel_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'op_admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    /**
     * 提现
     *
     * @param int $channel_id
     * @param float $total_fee
     * @param int $bank_id
     * @param int $admin_id
     * @return void
     * @author AriFe.Liu 
     */
    public function withdraw($channel_id,$total_fee,$bank_id,$admin_id){
        $withdraw_min = config('site.withdraw_min');
        $withdraw_max = config('site.withdraw_max');
        if($total_fee < $withdraw_min) exception('单次提现金额最低为: '.$withdraw_min.' 元');
        if($total_fee > $withdraw_max) exception('单次提现金额最高为: '.$withdraw_min.' 元');
        $bank = db('ChannelBank')->where(['id'=>$bank_id])->field("id,account,truename,mobile,bank_name")->find();
        # 判断余额是否充足
        $channel = db('Channel')->where(['id'=>$channel_id])->find();
        if(!$channel || $channel['status']!='1') exception('当前账号状态异常,无法申请提现');
        $balance = $channel['money'];
        if($balance < $total_fee){
            exception("您的余额不足!");
        }
        db()->startTrans();
        try {
            # 余额变动
            Index::money($channel_id,$total_fee,'余额提现',$admin_id);
            # 写提现记录
            $this->ins($channel_id,$admin_id,$total_fee,$bank['bank_name'],$bank['account'],$bank['truename'],$bank['mobile']);    
        } catch (\Exception $e) {
            db()->rollback();
            exception($e->getMessage());    
        }
        db()->commit();
        return true;
    }

    /**
     * 插入提现记录
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function ins($channel_id,$admin_id,$total_fee,$withdraw_type,$withdraw_account,$truename,$mobile){
        $res = $this->insert([
            'channel_id' => $channel_id,
            'createtime' => time(),
            'total_fee' => $total_fee,
            'withdraw_type' => $withdraw_type,
            'withdraw_account' => $withdraw_account,
            'truename' => $truename,
            'mobile' => $mobile,
            'status' => '0',
            'admin_id' => $admin_id,
        ]);
        if(!$res){
            exception('提现申请失败!(INS POSTER HAS ERROR)');
        }
    }

    /**
     * 获取概览
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function getOverview($admin_id){
        $channel_id = db('Channel')->where(['admin_id'=>$admin_id])->value('id');
        $overview = [
            'withdrawed' => 0,  # 已发起提现金额(非驳回的)
            'received' => 0,    # 已到账(已审核的)
            'unreceived' => 0,  # 未到账(未审核的)
        ];

        if($channel_id){
            $overview['withdrawed'] = $this->where(['channel_id'=>$channel_id,'status'=>['neq','2']])->sum('total_fee');
            $overview['received'] = $this->where(['channel_id'=>$channel_id,'status'=>'1'])->sum('total_fee');
            $overview['unreceived'] = $this->where(['channel_id'=>$channel_id,'status'=>'0'])->sum('total_fee');
        }else{
            $overview['withdrawed'] = $this->where(['status'=>['neq','2']])->sum('total_fee');
            $overview['received'] = $this->where(['status'=>'1'])->sum('total_fee');
            $overview['unreceived'] = $this->where(['status'=>'0'])->sum('total_fee');
        }
        
        return $overview;
    }
}
